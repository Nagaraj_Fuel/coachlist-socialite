<html lang="en">
    <head>
        <!--meta tags-->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        
        <title>Coachlist</title>
        <!--style files-->
        <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('/style.css')}}"/>
    </head>
    <body>
        <div class="navbar">
          <a href="#" class="brand-logo">Coachlist</a>
        </div>
        
        <div class="cl-container">
            <p class="cl-h4">Login</p>
            <a href="/login/facebook"><button class="loginBtn loginBtn--facebook">Login with Facebook</button></a>
        </div>
    </body>
</html>
